FROM docker.io/library/python:3.10
WORKDIR /app
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY main.py .
CMD ["python3","main.py"]
